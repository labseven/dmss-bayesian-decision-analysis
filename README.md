# Bayesian Decision Analysis

Midterm project for DMSS Fall 2020.

View the archive on gitlab [here](https://gitlab.com/labseven/dmss-bayesian-decision-analysis/-/blob/master/Report.ipynb), or live code hosted on google colab [here](https://colab.research.google.com/drive/1hF6HPZ91KW-kNt8zKDFJR3G1Db0ZRMp2).

